﻿using UnityEngine;
using System.Collections;

public abstract class MovingObject : MonoBehaviour {
	public float moveTime = 0.1f;
	
	private BoxCollider2D boxCollider;
	private Rigidbody2D rigidBody;
	private LayerMask collisionLayer;
	//private float inverseMoveTime;

	void Start () {
		boxCollider = GetComponent<BoxCollider2D> ();
		rigidBody = GetComponent<Rigidbody2D> ();
		collisionLayer = LayerMask.GetMask("Collision Layer");

		//inverseMoveTime = 1.0f / moveTime;
	}

	void Update () {
	
	}
}
